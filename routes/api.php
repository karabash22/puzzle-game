<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('login', static function () {
    dd('login');
})->name('login');

Route::post('auth', [\App\Http\Controllers\UserController::class, 'auth']);
Route::post('register', [\App\Http\Controllers\UserController::class, 'register']);

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('game')->group(function () {
        Route::post('', [GameController::class, 'start']);
        Route::post('/{game}/solve', [GameController::class, 'solve'])->middleware('can:update,game');
    });
});