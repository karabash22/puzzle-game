<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Models\Game;
use App\Contracts\GameServiceContract;
use App\Http\Requests\GameStartRequest;
use App\Http\Requests\GameSolveRequest;
use Carbon\Carbon;

class GameController extends Controller
{
    private $gameService;

    public function __construct(GameServiceContract $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * game start request
     * @param GameStartRequest $request
     */
    public function start(GameStartRequest $request)
    {
        $initialSequence = $request->exists('initial_sequence') ? $request->get('initial_sequence') : null;

        if (!$initialSequence) {
            $range = range(1, 15);
            do {
                shuffle($range);
                $initialSequence = implode(',',$range);
            } while ($this->gameService->checkInitialSequence($initialSequence));
        } else {
            $canSolved = $this->gameService->checkInitialSequence($initialSequence);

            if (!$canSolved) {
                throw new ApiException('The sequence is not solvable, input correct sequence');
            }
        }

        $gameFields = [
            'start_sequence' => $initialSequence,
            'user_id' => $request->user()->id
        ];

        $game = Game::create($gameFields);

        return response()->json([
            'game_id' => $game->id,
            'initial_sequence' => $initialSequence
        ]);
    }

    /**
     * game solve steps chain
     * @param Game $game
     * @param GameSolveRequest $request
     */
    public function solve(Game $game, GameSolveRequest $request)
    {
        $data = $request->validated();
        $result = $this->gameService->solveGame($game, $data['steps']);
        if ($result) {
            $game->solved_at = Carbon::now()->format('Y-m-d H:i:s');
            $game->save();
            $solvedAt = Carbon::createFromFormat('Y-m-d H:i:s', $game->solved_at);
            $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $game->created_at);
            $spent = $createdAt->diff($solvedAt)->format('%h hours, %i minutes, %s seconds');
            $response = [
                'result' => 'Congratulations! You are solve puzzle!',
                'spent' => $spent
            ];
        } else {
            $response = [
                'result' => 'Sorry, puzzle not solved, try again'
            ];
        }
        return response()->json($response);
    }
}
