<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiException;
use App\Http\Requests\UserAuthRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Ramsey\Collection\Collection;

class UserController extends Controller
{
    public function auth(UserAuthRequest $request)
    {
        $data = $request->validated();

        $user = User::query()->where('email', $data['email'])->firstOrFail();

        if (!Hash::check($data['password'], $user->password)) {
            throw new ApiException('Password incorrect');
        }

        $token =  $user->createToken($user->name)->plainTextToken;

        return response()->json([
            'access_token' => $token
        ], 200);
    }

    public function register(UserRegisterRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);

        $token = $user->createToken($data['name'])->plainTextToken;

        return response()->json([
            'access_token' => $token
        ], 200);

    }
}
