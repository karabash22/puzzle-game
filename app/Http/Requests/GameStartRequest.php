<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GameStartRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'initial_sequence' => [
                'sometimes',
                static function($attribute, $value, $fail) {
                    $values = explode(',', $value);
                    $range = range(1,15);
                    $unique = array_unique($values);
                    sort($unique);
                    if (count($unique) !== 15 || $range != $unique) {
                        $fail("{$attribute} field must contain random set of numbers from 1 to 15 splitted by comma");
                    }
                }
            ]
        ];
    }
}
