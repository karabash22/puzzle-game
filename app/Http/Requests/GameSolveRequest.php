<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameSolveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'steps' => 'required|array',
            'steps.*' => 'required|numeric|between:1,15'
        ];
    }
}
