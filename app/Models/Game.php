<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'start_sequence',
        'user_id',
        'solved_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
