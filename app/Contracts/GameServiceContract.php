<?php

namespace App\Contracts;

use App\Models\Game;

interface GameServiceContract
{
    public function solveGame(Game $game, array $steps);

}