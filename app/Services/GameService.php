<?php

namespace App\Services;

use App\Contracts\GameServiceContract;
use App\Models\Game;

class GameService implements GameServiceContract
{
    private $field;

    /**
     * @param Game $game
     * @param array $steps
     */
    public function solveGame(Game $game, array $steps)
    {
        $this->field = $this->getField($game->start_sequence);

        foreach ($steps as $step) {
            $this->nextStep($step);
        }
        $similarity = array_intersect_assoc(range(1, 15), $this->field);
        return count($similarity) === 15;
    }

    /**
     * Проверка решаемости последовательности
     * @param string $sequence
     * @return bool
     */
    public function checkInitialSequence(string $sequence): bool
    {
        $sum = 0;
        $range = range(1,15);
        $arr = explode(',', $sequence);

        foreach ($arr as $cell) {
            $counter = 0;
            foreach ($range as $num) {
                if ($cell > $num) {
                    $counter ++;
                }
            }
            $range = array_filter($range, function ($item) use ($cell) {
                return $item != $cell;
            });
            $sum += $counter;
        }
        $sum += 4;
        return $sum % 2 == 0;
    }

    private function vizualize()
    {
        dump("{$this->field[0]}, {$this->field[1]}, {$this->field[2]}, {$this->field[3]}");
        dump("{$this->field[4]}, {$this->field[5]}, {$this->field[6]}, {$this->field[7]}");
        dump("{$this->field[8]}, {$this->field[9]}, {$this->field[10]}, {$this->field[11]}");
        dump("{$this->field[12]}, {$this->field[13]}, {$this->field[14]}, {$this->field[15]}");
        dump('');
    }

    private function getField($start_sequence)
    {
        $field = explode(',', $start_sequence);
        $field[] = '*';
        return $field;
    }

    private function nextStep($step)
    {
        $field = $this->field;
        foreach ($field as $key => &$cell) {
            if ($cell != $step) continue;
            $swapKey = null;
            $neighboringCellKeys = [
                $key + 1,
                $key - 1,
                $key + 4,
                $key - 4
            ];

            foreach ($neighboringCellKeys as $neighboringCellKey) {
                if ($neighboringCellKey < 0) continue;
                $swapKey = array_key_exists($neighboringCellKey, $field)
                    && $field[$neighboringCellKey] === '*'
                    ? $neighboringCellKey : null;
                if (!is_null($swapKey)) {
                    $field[$swapKey] = $step;
                    $cell = '*';
                    break(2);
                }
            }
        }
        $this->field = $field;
    }
}