<?php


namespace App\Exceptions;


use Exception;
use Throwable;

class ApiException extends Exception
{
    private $errors;
    protected $message = 'Api error, look errors';

    /**
     * ApiException constructor.
     * @param string|array $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Api error", $code = 400, Throwable $previous = null)
    {
        $errors = is_array($message) ? $message : [$message];
        $this->setErrors($errors);

        parent::__construct($this->message, $code, $previous);
    }

    private function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function errors()
    {
        return $this->errors;
    }
}